

document.addEventListener("input", changeBackgroundColor)

function changeBackgroundColor () {
var newColor = document.getElementsByName("inputBackgroundColor")[0].value
console.log(newColor)
document.getElementById('background').style.setProperty("--background-color", newColor)

}


const fills = [
    '#9DCBBA',
    '#302B27',
    '#D0CFEC',
    '#FD5200',
    '#7C898B',
    '#086788',
    '#FFF1D0',
    '#D65780',
    '#4DA167',
    ]

const strokes = [
    '#6D5959',
    '#576CA8',
    '#6A8E7F',
    '#FE621D',
    '#4C443C',
    '#F0C808',
    '#DD1C1A',
    '#EE9480',
    '#3BC14A',
    ]
// arrays of colors for the random color picker to choose from

const strokeButton = document.getElementById('change-stroke-color')

strokeButton.addEventListener('click', changeStrokeColor)
// when the 'change-stroke-color' button is clicked, run the function 'changeStrokeColor':

function changeStrokeColor () {
  const maxIdx = strokes.length - 1
  const idx = Math.floor(Math.random() * Math.floor(maxIdx))
  const newColor = strokes[idx]
  document.getElementById('change-on-click').style.setProperty("--circle-stroke", newColor)

}

const fillButton = document.getElementById('change-fill-color')

fillButton.addEventListener('click', changeFillColor)

function changeFillColor () {
  const maxIdx = fills.length - 1
  const idx = Math.floor(Math.random() * Math.floor(maxIdx))
  const newColor = fills[idx]
  document.getElementById('change-on-click').style.setProperty("--circle-fill", newColor)
}
